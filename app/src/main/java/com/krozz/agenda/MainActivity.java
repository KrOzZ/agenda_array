package com.krozz.agenda;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final ArrayList<Contacto> listContacto = new ArrayList<>();
    private EditText txtNombre;
    private EditText txtTel1;
    private EditText txtTel2;
    private EditText txtDomicilio;
    private EditText txtNotas;
    private CheckBox cbFavorito;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnLista;
    private Button btnSalir;
    Contacto savedContent;
    int savedIndex;
    int index = 0;

    private boolean isEdit = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = findViewById(R.id.txtNombre);
        txtTel1 = findViewById(R.id.txtTel1);
        txtTel2 = findViewById(R.id.txtTel2);
        txtDomicilio = findViewById(R.id.txtDomicilio);
        txtNotas = findViewById(R.id.txtNotas);
        cbFavorito = findViewById(R.id.cbFavorito);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnLista = findViewById(R.id.btnLista);
        btnSalir = findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(this.btnSalirAction());
        btnGuardar.setOnClickListener(this.btnGuardarAction());
        btnLista.setOnClickListener(this.btnListarAction());
        btnLimpiar.setOnClickListener(this.btnLimpiarAction());

    }
    private View.OnClickListener btnSalirAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };
    }



    private View.OnClickListener btnListarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListaActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("contactos", listContacto);
                intent.putExtras(bundle);
                startActivityForResult(intent, 0);
            }
        };
    }

    private View.OnClickListener btnLimpiarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        };
    }
    private void limpiar(){
        txtDomicilio.setText("");
        txtNombre.setText("");
        txtTel1.setText("");
        txtTel2.setText("");
        txtNotas.setText("");
        cbFavorito.setChecked(false);
        savedContent = null;
        isEdit = false;
    }

    private View.OnClickListener btnGuardarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validar())
                {
                    Contacto contacto = new Contacto();
                    contacto.setNombre(getText(txtNombre));
                    contacto.setDireccion(getText(txtDomicilio));
                    contacto.setNotss(getText(txtNotas));
                    contacto.setTelefono(getText(txtTel1));
                    contacto.setTelefono2(getText(txtTel2));
                    contacto.setFavorito(cbFavorito.isChecked());
                    if( !isEdit)
                    {
                        contacto.setId(index);
                        listContacto.add(contacto);
                        index++;
                    }
                    else
                    {
                        contacto.setId(savedContent.getId());
                        editContact(contacto);
                        isEdit=false;
                        savedContent=null;
                    }
                    limpiar();

                    Toast.makeText(getApplicationContext(), R.string.mensaje,
                            Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(getApplicationContext(), R.string.msgerror,
                            Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void editContact(Contacto contacto)
    {
        for (int x = 0; x < this.listContacto.size(); x++)
        {
            if(this.listContacto.get(x).getId() == contacto.getId())
            {
                this.listContacto.set(x, contacto);
            }
        }
    }

    private boolean validar(){
        if(validarCampo(txtDomicilio) || validarCampo(txtNombre) ||
                validarCampo(txtNotas) || validarCampo(txtTel1) || validarCampo(txtTel2))
            return false;
        else
            return true;
    }


    private boolean validarCampo(EditText txt)
    {
        return txt.getText().toString().matches("");
    }

    private String getText(EditText txt)
    {
        return txt.getText().toString();
    }

    @Override
    protected void onActivityResult(int res, int result, Intent intent){
        if(intent != null)
        {
            Bundle bundle = intent.getExtras();
            int typeAction = bundle.getInt(getString(R.string.accion));

            this.listContacto.clear();
            this.listContacto.addAll((ArrayList<Contacto>)bundle.getSerializable(getString(R.string.listContactos)));
            if (typeAction==1) {
                savedContent = (Contacto) bundle.getSerializable("contacto");
                savedIndex = bundle.getInt("index");
                txtNombre.setText(savedContent.getNombre());
                txtNotas.setText(savedContent.getNotss());
                txtTel1.setText(savedContent.getTelefono());
                txtTel2.setText(savedContent.getTelefono2());
                txtDomicilio.setText(savedContent.getDireccion());
                cbFavorito.setChecked(savedContent.isFavorito());
                isEdit=true;
            }
        }
    }
}
