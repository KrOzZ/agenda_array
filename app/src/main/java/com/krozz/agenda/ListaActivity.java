package com.krozz.agenda;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ListaActivity extends AppCompatActivity {

    private TableLayout tbLista;
    private ArrayList<Contacto> contactos;
    private ArrayList<Contacto> filterContactos;
    private Button btnNuevo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_lista);

        tbLista = findViewById(R.id.tbList);
        btnNuevo = findViewById(R.id.btnNuevo);
        Bundle bundle = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundle.getSerializable("contactos");
        filterContactos = contactos;
        cargarContactos();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        btnNuevo.setOnClickListener(btnNuevo());


    }
    private View.OnClickListener btnNuevo()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData();
            }
        };
    }


    private void cargarContactos()
    {
        for(int x = 0; x < contactos.size(); x++)
        {
            final int position = x;
            final Contacto contacto = contactos.get(x);
            TableRow row = new TableRow(this);
            TextView lbNombre = new TextView(this);
            lbNombre.setText(contacto.getNombre());
            lbNombre.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            lbNombre.setTextColor((contacto.isFavorito()) ? Color.BLUE: Color.BLACK);
            row.addView(lbNombre);
            Button btnVer = new Button(this);
            btnVer.setText(R.string.accver);
            btnVer.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            btnVer.setTextColor(Color.BLACK);

            btnVer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto", (Contacto) v.getTag(R.string.contacto_g));
                    bundle.putSerializable(getString(R.string.listContactos),filterContactos);
                    i.putExtras(bundle);
                    setResult(RESULT_OK, i);
                    finish();
                }
            });

            btnVer.setTag(R.string.contacto_g, contacto);
            btnVer.setTag(R.string.index, x);
            row.addView(btnVer);
            tbLista.addView(row);
        }
    }

    public void buscar(String s)
    {
        ArrayList<Contacto> list = new ArrayList<>();
        for(int x = 0; x < filterContactos.size(); x++){
            if(filterContactos.get(x).getNombre().contains(s))
                list.add(filterContactos.get(x));
        }

        contactos = list;
        tbLista.removeAllViews();
        cargarContactos();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            //si el usuario presiona el boton back regresara la lista
            //pero no editara
            sendData();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void sendData()
    {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(getString(R.string.listContactos) ,this.filterContactos);
        bundle.putInt(getString(R.string.action), 0);
        intent.putExtras(bundle);
        setResult(Activity.RESULT_OK, intent);
        finish();
        Log.e(this.getClass().getName(), "back button pressed");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu,menu);
        MenuItem menuItem = menu.findItem(R.id.txtBuscar);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                buscar(newText);
                return false;
            }
        });
        return  super.onCreateOptionsMenu(menu);
    }
    private void elimianarContacto(long id)
    {
        for(int x = 0; x <filterContactos.size(); x++)
        {
            if(filterContactos.get(x).getId() == id)
            {
                filterContactos.remove(x);
                break;
            }
        }
        contactos = filterContactos;
        tbLista.removeAllViews();
        cargarContactos();

    }


}
