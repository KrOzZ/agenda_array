package com.krozz.agenda;

import java.io.Serializable;

public class Contacto implements Serializable {
    private long id;
    private String nombre;
    private String telefono;
    private String telefono2;
    private String direccion;
    private String notss;
    private boolean favorito;


    public Contacto(long id,String nombre, String telefono, String telefono2, String direccion, String notss, boolean favorito) {
        this.setId(id);
        this.setNombre(nombre);
        this.setTelefono(telefono);
        this.setTelefono2(telefono2);
        this.setDireccion(direccion);
        this.setNotss(notss);
        this.setFavorito(favorito);
    }

    public Contacto()
    {

    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNotss() {
        return notss;
    }

    public void setNotss(String notss) {
        this.notss = notss;
    }

    public boolean isFavorito() {
        return favorito;
    }

    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }
}
